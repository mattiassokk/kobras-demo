# Viktoriini Kobras veebirakendus

See repositoorium on loodud bakalaureusetöö "Veebirakenduse loomine informaatikaviktoriinile Kobras" raames, mille autor on Mattias Sokk.

Repositoorium sisaldab informaatikaviktoriini Kobras veebirakenduse lähtekoodi. Veebirakendus on loodud kasutades Vue.js JavaScripti veebiraamistikku. 

Kasutatud tehnoloogiad:
* Vue.js 
* Vite
* Quasar
* Vue Router
* TypeScript


CI/CD töö tulemusel on pandud rakenduse lähtefailid kokku veebileheks ning avalikustatud repositooriumi staatilise veebilehena aadressil [https://mattiassokk.gitlab.io/kobras-demo/](https://mattiassokk.gitlab.io/kobras-demo/). Veebirakendus kasutab kuvatavate andmetena [viktoriini põhilise repositooriumi](https://gitlab.com/mattiassokk/kobras-data/) poolt avalikustatud andmeid.


## Lokaalses arvutis veebirakenduse arendamine

**Nõuded:** Node.js (v16.14.0) ja Yarn (v1.22.11) olemasolu arvutis (sulgudes testitud versioonid).

* Sea endale üles viktoriini repositoorium, käivita sealne skript ja http-server ([juhend](https://gitlab.com/mattiassokk/kobras-data/-/tree/main/parser))
* Lae alla veebirakenudse repositoorium
* Ava repositooriumi juurkataloog
* Käivita kataloogis olles käsurealt käsk `yarn install` ja oota kuni kõik teegid on laetud
* Käivita samas kataloogis käsurealt käsk `npm run`
* Ava veebibrauser ning mine aadressile `http://localhost:3000/kobras-demo/`


## Staatilise veebilehe ehitamine (koos andmetega)

Käsitsi
* Ava selle repositooriumi juurkataloog
* Käivita kataloogis olles käsurealt käsk `yarn install` ja oota kuni kõik teegid on laetud
* Käivita samas kataloogis käsurealt käsk `yarn build -- --mode production`
* Selle repositooriumi juurkausta tekib kaust `dist`
* Sea endale üles viktoriini repositoorium ([kobras-data](https://gitlab.com/mattiassokk/kobras-data/)) ja käivita sealne skript [parse](https://gitlab.com/mattiassokk/kobras-data/-/tree/main/parser)
* Kopeeri skripti poolt teises repositooriumis genereeritud `public` kaust selle repositooriumi kausta `dist` ning nimeta kausta nimi `public` ümber nimeks `data` 
* Lae `dist` kasuta sisu üles veebiserverisse ning serveeri veebirakendust ja andmeid staatiliselt ühes kohas

Skriptiga
* Eelduseks on nii ülesannete ja veebirakenduse repositooriumite ühises kaustas olek:

```
|
|- kobras-demo
|- kobras-data
```
* Käivita skript `build_with_data.sh`
* Oota kuni skript lõpetab töö
* Lae `dist` kasuta sisu üles veebiserverisse ning serveeri veebirakendust ja andmeid staatiliselt ühes kohas