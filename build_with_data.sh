#!/bin/bash


if [ -d "dist" ]; then
  rm -rf "dist"
  echo "Deleted existing web build"
fi


echo "Installing required dependencies to build web application"
yarn -ci


echo "Building web application"
yarn build -- --mode production


if [ -d "../kobras-data/public" ]; then
  rm -rf "../kobras-data/public"
  echo "Deleted existing parsed data"
fi


cd ../kobras-data/parser
echo "Generating JSON data from data repository"
npm run parse


echo "Moving generated data to web application"
mv ../public ../../kobras-demo/dist/data/

