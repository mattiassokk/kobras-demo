import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { quasar, transformAssetUrls } from '@quasar/vite-plugin'

// https://vitejs.dev/config/

export default (mode: any) => {
    return defineConfig({

        server: {
            proxy: {
                "/local": {
                    target: "http://localhost:8080/",
                    changeOrigin: true,
                    secure: false,
                    rewrite: (path) => path.replace(/^\/dev/, "")
                },
                "/gitlab": {
                    target: "https://mattiassokk.gitlab.io/kobras-data/",
                    changeOrigin: true,
                    secure: false,
                    rewrite: (path) => path.replace(/^\/gitlab/, "")
                }
            },
        },
        build: {
            minify: true
        },

        css: { preprocessorOptions: { sass: { charset: false } } },
        plugins: [
            vue({
                template: { transformAssetUrls }
            }),

            quasar({
                sassVariables: 'src/quasar-variables.sass'
            })
        ]
    });
}
