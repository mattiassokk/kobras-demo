import {createApp} from 'vue'
import {Quasar} from 'quasar'
import mitt from 'mitt';

import '@quasar/extras/roboto-font-latin-ext/roboto-font-latin-ext.css'

import quasarIconSet from 'quasar/icon-set/mdi-v6'
import '@quasar/extras/mdi-v6/mdi-v6.css'

// Import Quasar css
import 'quasar/src/css/index.sass'

import App from './App.vue'
import router from './router'

const emitter = mitt();

const app = createApp(App)

app.use(Quasar, {
    iconSet: quasarIconSet,
    config: {
        brand: {
            'primary': '#21a7e1',
            'secondary': '#26A69A',
            'accent': '#9C27B0',
            'dark': '#1d1d1d',
            'dark-page': '#121212',
            'positive': '#21BA45',
            'negative': '#C10015',
            'info': '#31CCEC',
            'warning': '#F2C037'
        }
    }
})

// Connect mitt
app.config.globalProperties.emitter = emitter;

// Connect Vue Router
app.use(router);

// Assumes you have a <div id="app"></div> in your index.html
app.mount('#app')
