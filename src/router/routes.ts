import {RouteRecordRaw} from 'vue-router';


const routes: RouteRecordRaw[] =
    [
        {
            name: 'print',
            path: '/print',
            component: () => import('../layouts/PrintLayout.vue'),
        },
        {
            name: "main",
            path: '/',
            component: () => import('../layouts/MainLayout.vue'),
            children: [
                {
                    name: 'home',
                    path: '',
                    component: () => import('../views/HomeView.vue')
                },
                {
                    name: 'tasks',
                    path: '/tasks',
                    component: () => import('../views/TasksView.vue')
                },
                {
                    name: 'quiz',
                    path: '/quiz',
                    component: () => import('../views/QuizView.vue')
                },
                {
                    name: 'results',
                    path: '/results',
                    component: () => import('../views/ResultsView.vue')
                },
                {
                    name: 'history',
                    path: '/history',
                    component: () => import('../views/HistoryView.vue')
                },
                {
                    name: 'guide',
                    path: '/guide',
                    component: () => import('../views/GuideView.vue')
                },
                {
                    path: "/:catchAll(.*)",
                    redirect: "/"
                }
            ]
        },
    ];

export default routes;