export default interface ResultIndex {
    year: string,
    round: number,
    ageCat: string,
    path: string
}