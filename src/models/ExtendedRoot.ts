import {Vue} from "vue-class-component";

export default interface ExtendedVueRoot extends Vue {
    selectedLang: string,
    getLocaleText: any
}