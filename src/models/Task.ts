import Question from "./questions/Question";
import {AnswerState} from "./questions/AnswerState";

export default interface Task {
    no: number[],
    round: number,
    year: number,
    title: string,
    description: string,
    questions: Question[],
    explanation?: string,
    informatics?: string,
    state?: AnswerState
}