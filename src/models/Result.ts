export default interface Result {
    place: number,
    name: string,
    school: string,
    grade: string,
    points: string,
    notes: string
}