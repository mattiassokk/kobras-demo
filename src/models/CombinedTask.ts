import Task from "./Task";
import TaskIndex from "./TaskIndex";

export default interface CombinedTask extends Task, TaskIndex {}