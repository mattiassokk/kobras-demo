import Question from "./Question";

export default interface IntegerQuestion extends Question {
    form: {
      start?: number,
      end?: number
    },
    answer: number[]
}