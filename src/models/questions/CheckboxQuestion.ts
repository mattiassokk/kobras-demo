import Question from "./Question";

export default interface CheckboxQuestion extends Question {
    form: [
        {
            value: string,
            label: string
        }
    ],
    answer: string[][]
}