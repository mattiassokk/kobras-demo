import Question from "./Question";

export default interface RadioQuestion extends Question {
    form: [
        {
            value: string,
            label: string
        }
    ]
    answer: string[]
}