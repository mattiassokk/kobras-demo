import Question from "./Question";

export default interface MatchQuestion extends Question {
    form: [
        {
            value: string,
            label: string,
            matches: string[]
        }
    ],
    answer: any[]
}