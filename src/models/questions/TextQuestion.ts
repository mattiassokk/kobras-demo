import Question from "./Question";

export default interface TextQuestion extends Question {
    answer: string[]
}