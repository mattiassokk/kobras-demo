import {AnswerState} from "./AnswerState";

export default interface Question {
    type: string,
    question: string,
    form: any,
    answer: any[],
    userAnswer?: any,
    enabled: boolean,
    state?: AnswerState,
    solvable: boolean
}