export enum AnswerState {
    EMPTY,
    CORRECT,
    INCORRECT
}