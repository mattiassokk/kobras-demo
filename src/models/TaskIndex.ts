export default interface TaskIndex {
    id: number,
    no: number[],
    title: string
    round: number,
    year: number,
    ageCat: string[],
    country: string,
    categories: string[],
    path: string
}